#include<iostream>
#include<random>
#include<math.h>
#include<ctime>
#include <fstream>
#include <unordered_set>
#include <set>
#include <vector>
#include <assert.h>

using namespace std;

const int vectors[2][4][4] =      { { {-1,-1, 1,0},{ 1,-1,-1,0},{ 1, 1, 1,0},{-1, 1,-1,0} }, 
                                    {   { 1, 1,-1,0},{-1, 1, 1,0},{-1,-1,-1,0},{ 1,-1, 1,0} } };
// randomly select next vector of random walk excluding going back (wrong for
// SAW statistics?)
// numbers are coordinates of vectors[][x][]
const unsigned short int vec_choose[4][3] = {
    {   1, 2, 3   },
    {   0, 2, 3   },
    {   0, 1, 3   },
    {   0, 1, 2   } };

// updates after rotations
// + rot = rot_vec, -rot = transpose(rot_vec) (say + is clockwise,
// - anticlockwise, however dont know which is which and it is not important
// since there is only two options [++ = -, -- = +, +++/--- = 0] (excluding +++ (---) = 3*120 angle)
//
const unsigned short int rot_vec[4][4] = {{0, 2, 3, 1},
                                          {3, 1, 0, 2}, 
                                          {1, 3, 2, 0}, 
                                          {2, 0, 1, 3} };


random_device ran;
mt19937_64 gen(ran());
uniform_int_distribution<> r_int2(0,2); // randomly choose next vector (excluding back vec)
uniform_int_distribution<> r_int3(0,3); // randomly choose next vector (including back vec)
uniform_int_distribution<> r_int1(0,1); // randomly choose rotation {1,2}*120

struct point
{
    int l; // position
    unsigned short int s; // direction
    int hash;
    point * prev;
    point * next;
    int coords[4]; // 3rd is vector, 0-2 - xyz coordinates

    point (){
        prev = this -1;
        next = this +1;
    }
    void up_vec(int v) {
        for (int i=0; i<4;i++)  // vectorized
            coords[i] = prev->coords[i] + vectors[s][v][i];
        coords[3] = v;
        hash = abs(47*coords[0] + 2210*coords[1] + 103824*coords[2])%5000000;
    }
    point (int n, int vec, point * pr) {
        l = n;
        s = n%2;
        for (int i=0; i<4;i++)  // vectorized
            coords[i] = pr->coords[i] + vectors[s][vec][i];
        coords[3] = vec;
        hash = abs(47*coords[0] + 2210*coords[1] + 103824*coords[2])%5000000;
        
    }
    int distance(point * p2) {
        int x = this->coords[0] - p2->coords[0];
        int y = this->coords[1] - p2->coords[1];
        int z = this->coords[2] - p2->coords[2];
        return int(sqrt(x*x + y*y + z*z));
    }
    void rotate(int anchor, int direction, bool forward) {
        unsigned short int n;

        if (direction==0) // TODO change for backward
            n = rot_vec[anchor][coords[3]];
        else
            n = rot_vec[coords[3]][anchor];

        if (forward) {
            for (int i=0; i<4;i++){  // vectorized
                coords[i] = prev->coords[i] + vectors[s][n][i];
            }
        } else {
            int nk = next->coords[3];
            for (int i=0; i<4;i++)  {// vectorized
                coords[i] = next->coords[i] - vectors[!s][nk][i];
            }
        }

        coords[3] = n;
        hash = abs(47*coords[0] + 2210*coords[1] + 103824*coords[2])%5000000;

    }

};
struct Hash {
    size_t operator() (const point &p) const  {
        return p.hash;
    }
};
inline bool operator == (point const& p1, point const& p2) {
    return (p1.coords[0] == p2.coords[0]) && 
            (p1.coords[1] == p2.coords[1]) && 
            (p1.coords[2] == p2.coords[2]);
}
struct Chain
{
    int len;
    point * chain;
    std::unordered_set<point, Hash> h;
    Chain(int N) {
        len = N;
        chain = new point[N];
        for (int i=0;i<N;i++) {
            chain[i].s = i%2;
            chain[i].l = i;
        }
        chain[0].prev = nullptr;
        chain[N-1].next = nullptr;
    }
    Chain(Chain *ch) {
        len = ch->len;
        chain = new point[len];
        for (int i=0;i<len;i++) {
            chain[i] = ch->chain[i];
            h.insert(chain[i]);
        }
        for (int i=0;i<len;i++) {
            chain[i].prev = &chain[i-1];
            chain[i].next = &chain[i+1];
        }
        chain[0].prev = nullptr;
        chain[len-1].next = nullptr;
    }

    void insertPoint(int n, point p) {
        this->h.erase(chain[n]);
        this->chain[n] = p;

        if (n>0)
            this->chain[n].prev = &chain[n-1];
        if (n<this->len-1) {
            this->chain[n].next = &chain[n+1];
//            chain[n+1].prev = &chain[n]; // TODO ????
        }
        if (n==this->len-1)
            this->chain[n].next = nullptr;
        if (n==0)
            this->chain[n].prev = nullptr;

        this->h.insert(p);
    }
    bool tryMove(int n) { // ????? TODO
        point * pr = &this->chain[n-1];
        int rand_vec = r_int3(gen);
        point p = point(n, rand_vec, pr);
        if (h.find(p) == h.end() ) {
            h.insert(p);
            insertPoint(n, p);
            return true;
        }
        return false;
    }

    point emit(int n) {
        assert(n>len || n<0);
        return chain[n];
    }
        
    void print() {
        for (int i=0;i<len;i++) {
            for (int j=0;j<3;j++) 
                cout << chain[i].coords[j] << " ";
            cout << endl;
        }
        cout << endl << endl << endl;
    }
    void print(int from, int to) {
        for (int i=from;i<to+1;i++) {
            for (int j=0;j<3;j++) 
                cout << chain[i].coords[j] << " ";
            cout << endl;
        }
        cout << endl << endl << endl;
    }
    
    ~Chain() {
        if (chain)
            delete [] chain;
    }
    void straight_chain() {
        // 0 is defined as 0 0 0 
        for (int i=1; i<len; i++) {
            chain[i].up_vec(i%2);
            h.insert(chain[i]);
        }
    }
    void random_walk() { // TODO insert into set
        // 0 is defined as 0 0 0 
        for (int i=1; i<len; i++) 
            chain[i].up_vec(r_int3(gen));
    }
    void selfavoiding_walk(int max_tries) {
        while (max_tries>0) {
        max_tries--; 
            skip:
            h.clear();
            h.insert(chain[0]);
            int mm = 1;
            while (mm<len) { // from 1 to N, not N->1
                if (tryMove(mm) != true)
                    goto skip;
                mm++;
            }

        }
    }
    bool pivot(int cycles) {
        uniform_int_distribution<> r_intN(1,len-1); 
        for (int c=0;c<cycles;c++) {
            int random_place = r_intN(gen);
            int direction = r_int1(gen);
            int anchor = chain[random_place].coords[3]; // vector 

            if (random_place>int(len/2))  {
                for (int i=random_place;i<len;i++)  {
                    chain[i].rotate(anchor, direction, true); // forward direction
                }
            }
            else {
                for (int i=random_place;i>-1;i--) {
                    chain[i].rotate(anchor, direction, false); // backward direction
                }
            }

        }
        // until now doesnt check crossings. imho better chance of knotting (or
        // not, just guessing)
        h.clear();
        for (int i=0;i<len;i++) {
            if (h.find(chain[i]) == h.end()) {
                h.insert(chain[i]);
            }
            else {
                return false;
            }
        }
        return true;

    }
    
};

int main() {
Chain ch = Chain(230);
//ch.selfavoiding_walk(11000);
ch.straight_chain();
ch.print();

Chain ch2 = Chain(&ch);
int g = 1000000;
while (g>0) {
    if (ch2.pivot(180)) {
        ch2.print();
        g--;
    }
}
}

